package com.example.mywechat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import java.util.Calendar;

public class returnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return);
        Intent intent=getIntent();
        intent.putExtra("data","今天是安卓课程。"+"\n"+"上课时间为"+"\n"+ Calendar.getInstance().getTime());
        setResult(666,intent);
        finish();
    }
}